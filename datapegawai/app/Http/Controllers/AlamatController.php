<?php

namespace App\Http\Controllers;

use App\Models\Alamat;
use App\Models\Pegawai;
use Illuminate\Http\Request;

class AlamatController extends Controller
{
    // Halaman untuk menambahkan data alamat
    public function create(Pegawai $pegawai)
    {
        return view('alamat.create', compact('pegawai'));
    }

    // Menyimpan data alamat
    public function store(Request $request)
    {
        $validated = $request->validate([
            'pegawai_id' => 'required',
            'jalan_nomor' => 'required|max:100',
            'rt' => 'nullable|max:5',
            'rw' => 'nullable|max:5',
            'kelurahan' => 'required|max:100',
            'kecamatan' => 'required|max:100',
            'kota' => 'required|max:100',
            'provinsi' => 'required|max:100',
        ]);

        $alamat = Alamat::create($validated);

        return redirect()->route('pegawai.show', ['pegawai' => $alamat->pegawai]);
    }

    // Menampilkan halaman untuk mengedit alamat
    public function edit(Pegawai $pegawai)
    {
        return view('alamat.edit')->with('alamat', $pegawai->alamat);
    }

    // Menyimpan perubahan alamat
    public function update(Request $request, Pegawai $pegawai)
    {
        $validated = $request->validate([
            'jalan_nomor' => 'required|max:100',
            'rt' => 'nullable|max:5',
            'rw' => 'nullable|max:5',
            'kelurahan' => 'required|max:100',
            'kecamatan' => 'required|max:100',
            'kota' => 'required|max:100',
            'provinsi' => 'required|max:100',
        ]);

        $pegawai->alamat->update($validated);

        return redirect()->route('pegawai.show', ['pegawai' => $pegawai]);
    }

    // Menghapus data alamat
    public function destroy(Pegawai $pegawai)
    {
        $pegawai->alamat->delete();

        return redirect()->route('pegawai.show', ['pegawai' => $pegawai]);
    }
}
