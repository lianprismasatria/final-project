<?php

namespace App\Http\Controllers;

use App\Models\DataLain;
use App\Models\Pegawai;
use Illuminate\Http\Request;

class DataLainController extends Controller
{
    // Halaman untuk menambahkan data datalain
    public function create(Pegawai $pegawai)
    {
        return view('datalain.create', compact('pegawai'));
    }

    // Menyimpan data datalain
    public function store(Request $request)
    {
        $validated = $request->validate([
            'pegawai_id' => 'required',
            'nomor' => 'required|max:100',
            'keterangan' => 'required|max:255',
        ]);

        $datalain = DataLain::create($validated);

        return redirect()->route('pegawai.show', ['pegawai' => $datalain->pegawai]);
    }

    // Halaman edit data datalain
    public function edit(DataLain $datalain)
    {
        return view('datalain.edit', compact('datalain'));
    }

    // Menyimpan perubahan data datalain
    public function update(Request $request, DataLain $datalain)
    {
        $validated = $request->validate([
            'nomor' => 'required|max:100',
            'keterangan' => 'required|max:255',
        ]);

        $datalain->update($validated);

        return redirect()->route('pegawai.show', ['pegawai' => $datalain->pegawai]);
    }

    // Menghapus data datalain
    public function destroy(DataLain $datalain)
    {
        $pegawai = $datalain->pegawai;
        $datalain->delete();

        return redirect()->route('pegawai.show', ['pegawai' => $pegawai]);
    }
}
