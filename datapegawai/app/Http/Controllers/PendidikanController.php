<?php

namespace App\Http\Controllers;

use App\Models\Pegawai;
use App\Models\Pendidikan;
use Illuminate\Http\Request;

class PendidikanController extends Controller
{
    // Halaman untuk menambahkan data pendidikan
    public function create(Pegawai $pegawai)
    {
        return view('pendidikan.create', compact('pegawai'));
    }

    // Menyimpan data pendidikan
    public function store(Request $request)
    {
        $validated = $request->validate([
            'pegawai_id' => 'required',
            'jenjang' => 'required|max:100',
            'nama_sekolah' => 'required|max:255',
            'jurusan' => 'nullable|max:255',
            'tahun_lulus' => 'required|max:4',
            'nomor_ijazah' => 'required|max:100',
        ]);

        $pendidikan = Pendidikan::create($validated);

        return redirect()->route('pegawai.show', ['pegawai' => $pendidikan->pegawai]);
    }

    // Halaman edit data pendidikan
    public function edit(Pendidikan $pendidikan)
    {
        return view('pendidikan.edit', compact('pendidikan'));
    }

    // Menyimpan perubahan data pendidikan
    public function update(Request $request, Pendidikan $pendidikan)
    {
        $validated = $request->validate([
            'jenjang' => 'required|max:100',
            'nama_sekolah' => 'required|max:255',
            'jurusan' => 'nullable|max:255',
            'tahun_lulus' => 'required|max:4',
            'nomor_ijazah' => 'required|max:100',
        ]);

        $pendidikan->update($validated);

        return redirect()->route('pegawai.show', ['pegawai' => $pendidikan->pegawai]);
    }

    // Menghapus data pendidikan
    public function destroy(Pendidikan $pendidikan)
    {
        $pegawai = $pendidikan->pegawai;
        $pendidikan->delete();

        return redirect()->route('pegawai.show', ['pegawai' => $pegawai]);
    }
}
