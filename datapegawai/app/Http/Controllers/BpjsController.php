<?php

namespace App\Http\Controllers;


use App\Models\Bpjs;
use App\Models\Pegawai;
use Illuminate\Http\Request;

class BpjsController extends Controller
{
    // Halaman untuk menambahkan data bpjs
    public function create(Pegawai $pegawai)
    {
        return view('bpjs.create', compact('pegawai'));
    }

    // Menyimpan data bpjs
    public function store(Request $request)
    {
        $validated = $request->validate([
            'pegawai_id' => 'required',
            'kesehatan' => 'required|max:100',
            'ketenagakerjaan' => 'required|max:100',
        ]);

        $bpjs = Bpjs::create($validated);

        return redirect()->route('pegawai.show', ['pegawai' => $bpjs->pegawai]);
    }

    // Menampilkan halaman untuk mengedit bpjs
    public function edit(Pegawai $pegawai)
    {
        return view('bpjs.edit')->with('bpjs', $pegawai->bpjs);
    }

    // Menyimpan perubahan bpjs
    public function update(Request $request, Pegawai $pegawai)
    {
        $validated = $request->validate([
            'kesehatan' => 'required|max:100',
            'ketenagakerjaan' => 'required|max:100',
        ]);

        $pegawai->bpjs->update($validated);

        return redirect()->route('pegawai.show', ['pegawai' => $pegawai]);
    }

    // Menghapus data bpjs
    public function destroy(Pegawai $pegawai)
    {
        $pegawai->bpjs->delete();

        return redirect()->route('pegawai.show', ['pegawai' => $pegawai]);
    }
}
