<?php

namespace App\Http\Controllers;

use App\Models\Pegawai;
use Illuminate\Http\Request;

class PegawaiController extends Controller
{
    // Tampilkan semua data pegawai
    public function index()
    {
        $pegawai = Pegawai::all();

        return view('pegawai.index', compact('pegawai'));
    }

    // Tampilan untuk menambahkan data pegawai
    public function create()
    {
        return view('pegawai.create');
    }

    // Menyimpan data yang diinput dari form
    public function store(Request $request)
    {
        $validated = $request->validate([
            'nama' => 'required|max:255',
            'tempat_lahir' => 'required|max:255',
            'tanggal_lahir' => 'required',
            'jenis_kelamin' => 'required|max:10',
            'golongan_darah' => 'required|max:10',
            'agama' => 'required|max:100',
            'status_perkawinan' => 'required|max:100',
            'kewarganegaraan' => 'required|max:255',
            'nik' => 'max:16',
            'kk' => 'max:16',
            'npwp' => 'max:20',
        ]);

        Pegawai::create($validated);

        return redirect()->route('pegawai.index');
    }

    // Menampilkan data pegawai per-orang
    public function show(Pegawai $pegawai)
    {
        return view('pegawai.show', compact('pegawai'));
    }

    // Menampilkan halaman untuk edit data pegawai
    public function edit(Pegawai $pegawai)
    {
        return view('pegawai.edit', compact('pegawai'));
    }

    // Memproses pengubahan data pegawai
    public function update(Request $request, Pegawai $pegawai)
    {
        $validated = $request->validate([
            'nama' => 'required|max:255',
            'tempat_lahir' => 'required|max:255',
            'tanggal_lahir' => 'required',
            'jenis_kelamin' => 'required|max:10',
            'golongan_darah' => 'required|max:10',
            'agama' => 'required|max:100',
            'status_perkawinan' => 'required|max:100',
            'kewarganegaraan' => 'required|max:255',
            'nik' => 'max:16',
            'kk' => 'max:16',
            'npwp' => 'max:20',
        ]);

        $pegawai->update($validated);

        return redirect()->route('pegawai.show', ['pegawai' => $pegawai]);
    }

    // Menghapus data pegawai
    public function destroy(Pegawai $pegawai)
    {
        $pegawai->delete();

        return redirect()->route('pegawai.index');
    }
}
