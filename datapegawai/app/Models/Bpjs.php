<?php

namespace App\Models;

use App\Models\Pegawai;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Bpjs extends Model
{
    use HasFactory;

    protected $table = 'bpjs';

    protected $guarded = [];

    // Atribut yang akan diubah menjadi tipe native
    protected $casts = [
        'id' => 'integer',
        'pegawai_id' => 'integer',
    ];

    // Relasi dengan pegawai
    public function pegawai()
    {
        return $this->belongsTo(Pegawai::class);
    }
}
