<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pegawai extends Model
{
    protected $table = 'pegawai';
    
    protected $guarded = [];

    // Atribut akan diubah menjadi tipe native
    protected $casts = [
        'id' => 'integer',
        'tanggal_lahir' => 'date',
    ];

    // Relasi dengan user
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    // Relasi dengan alamat
    public function alamat()
    {
        return $this->hasOne(Alamat::class);
    }

    // Relasi dengan pendidikan
    public function pendidikan()
    {
        return $this->hasMany(Pendidikan::class);
    }

    // Relasi dengan data bpjs
    public function bpjs()
    {
        return $this->hasOne(Bpjs::class);
    }

    // Relasi dengan data lainnya
    public function datalain()
    {
        return $this->hasMany(DataLain::class);
    }
}
