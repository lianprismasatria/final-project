<?php

namespace App\Models;

use App\Models\Pegawai;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Pendidikan extends Model
{
    use HasFactory;

    protected $table = 'pendidikan';

    protected $guarded = [];

    // Atribut yang akan diubah menjadi tipe native
    protected $casts = [
        'id' => 'integer',
        'pegawai_id' => 'integer',
        'tanggal_lulus' => 'date',
    ];

    // Relasi dengan pegawai
    public function pegawai()
    {
        return $this->belongsTo(Pegawai::class);
    }
}
