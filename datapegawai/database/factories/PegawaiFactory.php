<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use App\Models\Pegawai;

class PegawaiFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Pegawai::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'nama' => $this->faker->word,
            'tempat_lahir' => $this->faker->word,
            'tanggal_lahir' => $this->faker->date(),
            'jenis_kelamin' => $this->faker->word,
            'golongan_darah' => $this->faker->word,
            'agama' => $this->faker->word,
            'status_perkawinan' => $this->faker->word,
            'kewarganegaraan' => $this->faker->word,
            'nik' => $this->faker->word,
            'kk' => $this->faker->word,
            'npwp' => $this->faker->word,
        ];
    }
}
