<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use App\Models\Bpjs;
use App\Models\Pegawai;

class BpjsFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Bpjs::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'pegawai_id' => Pegawai::factory(),
            'kesehatan' => $this->faker->word,
            'ketenagakerjaan' => $this->faker->word,
        ];
    }
}
