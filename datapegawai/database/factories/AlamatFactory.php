<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use App\Models\Alamat;
use App\Models\Pegawai;

class AlamatFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Alamat::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'pegawai_id' => Pegawai::factory(),
            'jalan_nomor' => $this->faker->word,
            'rt' => $this->faker->word,
            'rw' => $this->faker->word,
            'kelurahan' => $this->faker->word,
            'kecamatan' => $this->faker->word,
            'kota' => $this->faker->word,
            'provinsi' => $this->faker->word,
        ];
    }
}
