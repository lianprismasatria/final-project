<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use App\Models\DataLain;
use App\Models\Pegawai;

class DataLainFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = DataLain::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'pegawai_id' => Pegawai::factory(),
            'nomor' => $this->faker->word,
            'keterangan' => $this->faker->word,
        ];
    }
}
