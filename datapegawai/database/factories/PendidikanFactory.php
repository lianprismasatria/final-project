<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use App\Models\Pegawai;
use App\Models\Pendidikan;

class PendidikanFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Pendidikan::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'pegawai_id' => Pegawai::factory(),
            'jenjang' => $this->faker->word,
            'nama_sekolah' => $this->faker->word,
            'jurusan' => $this->faker->word,
            'tanggal_lulus' => $this->faker->date(),
            'nomor_ijazah' => $this->faker->word,
        ];
    }
}
