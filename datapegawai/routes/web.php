<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PegawaiController;
use App\Http\Controllers\AlamatController;
use App\Http\Controllers\PendidikanController;
use App\Http\Controllers\BpjsController;
use App\Http\Controllers\DataLainController;

Route::middleware(['auth:sanctum', 'verified'])->get('/', function () {
    return view('dashboard');
})->name('dashboard');


Route::resource('pegawai', PegawaiController::class)->middleware('auth:sanctum');

Route::post('/pegawai/{pegawai}/alamat', [AlamatController::class, 'store'])->middleware('auth:sanctum')->name('pegawai.alamat.store');
Route::put('/pegawai/{pegawai}/alamat', [AlamatController::class, 'update'])->middleware('auth:sanctum')->name('pegawai.alamat.update');
Route::get('/pegawai/{pegawai}/alamat/create', [AlamatController::class, 'create'])->middleware('auth:sanctum')->name('pegawai.alamat.create');
Route::get('/pegawai/{pegawai}/alamat/edit', [AlamatController::class, 'edit'])->middleware('auth:sanctum')->name('pegawai.alamat.edit');
Route::delete('/pegawai/{pegawai}/alamat/delete', [AlamatController::class, 'destroy'])->middleware('auth:sanctum')->name('pegawai.alamat.destroy');

Route::put('/pegawai/pendidikan/{pendidikan}', [PendidikanController::class, 'update'])->middleware('auth:sanctum')->name('pegawai.pendidikan.update');
Route::get('/pegawai/pendidikan/{pendidikan}/edit', [PendidikanController::class, 'edit'])->middleware('auth:sanctum')->name('pegawai.pendidikan.edit');
Route::delete('/pegawai/pendidikan/{pendidikan}/delete', [PendidikanController::class, 'destroy'])->middleware('auth:sanctum')->name('pegawai.pendidikan.destroy');
Route::post('/pegawai/{pegawai}/pendidikan', [PendidikanController::class, 'store'])->middleware('auth:sanctum')->name('pegawai.pendidikan.store');
Route::get('/pegawai/{pegawai}/pendidikan/create', [PendidikanController::class, 'create'])->middleware('auth:sanctum')->name('pegawai.pendidikan.create');

Route::post('/pegawai/{pegawai}/bpjs', [BpjsController::class, 'store'])->middleware('auth:sanctum')->name('pegawai.bpjs.store');
Route::put('/pegawai/{pegawai}/bpjs', [BpjsController::class, 'update'])->middleware('auth:sanctum')->name('pegawai.bpjs.update');
Route::get('/pegawai/{pegawai}/bpjs/create', [BpjsController::class, 'create'])->middleware('auth:sanctum')->name('pegawai.bpjs.create');
Route::get('/pegawai/{pegawai}/bpjs/edit', [BpjsController::class, 'edit'])->middleware('auth:sanctum')->name('pegawai.bpjs.edit');
Route::delete('/pegawai/{pegawai}/bpjs/delete', [BpjsController::class, 'destroy'])->middleware('auth:sanctum')->name('pegawai.bpjs.destroy');

Route::put('/pegawai/datalain/{datalain}', [DataLainController::class, 'update'])->middleware('auth:sanctum')->name('pegawai.datalain.update');
Route::get('/pegawai/datalain/{datalain}/edit', [DataLainController::class, 'edit'])->middleware('auth:sanctum')->name('pegawai.datalain.edit');
Route::delete('/pegawai/datalain/{datalain}/delete', [DataLainController::class, 'destroy'])->middleware('auth:sanctum')->name('pegawai.datalain.destroy');
Route::post('/pegawai/{pegawai}/datalain', [DataLainController::class, 'store'])->middleware('auth:sanctum')->name('pegawai.datalain.store');
Route::get('/pegawai/{pegawai}/datalain/create', [DataLainController::class, 'create'])->middleware('auth:sanctum')->name('pegawai.datalain.create');
