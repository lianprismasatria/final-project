<x-app-layout>
  <x-slot name="header">
    <h2 class="font-semibold text-xl text-gray-800 leading-tight">Pegawai</h2>
    <div class="mt-3 sm:mt-0 sm:ml-4">
      <a
        href="{{ route('pegawai.create') }}"
        type="button"
        class="inline-flex items-center px-4 py-2 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
      >
        Tambah
      </a>
    </div>
  </x-slot>

  <div class="py-12">
    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
      <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
        <div class="flex flex-col">
          <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
            <div
              class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8"
            >
              <div
                class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg"
              >
                <table class="min-w-full divide-y divide-gray-200">
                  <thead class="bg-gray-50">
                    <tr>
                      <th
                        scope="col"
                        class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                      >
                        Nama
                      </th>
                      <th
                        scope="col"
                        class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                      >
                        Tempat, tanggal lahir
                      </th>
                      <th
                        scope="col"
                        class="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                      >
                        Jenis kelamin
                      </th>
                      <th scope="col" class="relative px-6 py-3">
                        <span class="sr-only">Lihat</span>
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach ($pegawai as $person)
                      <tr
                        class="bg-white @if($loop->iteration % 2 == 0) bg-gray-50 @endif"
                      >
                        <td
                          class="px-6 py-4 whitespace-nowrap text-sm font-medium text-gray-900"
                        >
                          {{ $person->nama }}
                        </td>
                        <td
                          class="px-6 py-4 whitespace-nowrap text-sm text-gray-500"
                        >
                          {{ $person->tempat_lahir }}, {{
                          $person->tanggal_lahir->locale('id')->isoFormat('LL') }}
                        </td>
                        <td
                          class="px-6 py-4 whitespace-nowrap text-sm text-gray-500"
                        >
                          {{ $person->jenis_kelamin }}
                        </td>
                        <td
                          class="px-6 py-4 whitespace-nowrap text-right text-sm font-medium"
                        >
                          <a
                            href="{{ route('pegawai.show', ['pegawai' => $person->id]) }}"
                            class="text-indigo-600 hover:text-indigo-900"
                            >Lihat</a
                          > | <a
                            href="{{ route('pegawai.edit', ['pegawai' => $person->id]) }}"
                            class="text-indigo-600 hover:text-indigo-900"
                            >Edit</a
                          >
                        </td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</x-app-layout>
