<div class="pt-12">
  <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
    <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
      <div class="bg-white shadow overflow-hidden sm:rounded-lg">
        @empty ($pegawai->alamat)
          <div
            class="px-4 py-5 sm:px-6 sm:flex sm:items-center sm:justify-between"
          >
            <p class="text-lg leading-6 font-medium text-gray-900">
              Tidak ada data alamat.
              <a
                href="{{ route('pegawai.alamat.create', ['pegawai' => $pegawai->id]) }}"
                class="text-indigo-600 hover:text-indigo-900"
                >Tambahkan</a
              >
            </p>
          </div>
        @else
          <div
            class="px-4 py-5 sm:px-6 sm:flex sm:items-center sm:justify-between"
          >
            <h3 class="text-lg leading-6 font-medium text-gray-900">
              Alamat
            </h3>
            <a
              href="{{ route('pegawai.alamat.edit', ['pegawai' => $pegawai->id]) }}"
              type="button"
              class="inline-flex items-center px-4 py-2 border border-transparent rounded-md shadow-sm text-sm font-medium text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
            >
              Edit
            </a>
          </div>
          <div class="border-t border-gray-200 px-4 py-5 sm:p-0">
            <dl class="sm:divide-y sm:divide-gray-200">
              <div class="py-4 sm:py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                <dt class="text-sm font-medium text-gray-500">Alamat</dt>
                <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                  {{ $pegawai->alamat->jalan_nomor }}
                </dd>
              </div>
              <div class="py-4 sm:py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                <dt class="text-sm font-medium text-gray-500">RT / RW</dt>
                <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                  {{ $pegawai->alamat->rt ? $pegawai->alamat->rt : '-' }} / {{ $pegawai->alamat->rw ? $pegawai->alamat->rw : '-'}}
                </dd>
              </div>
              <div class="py-4 sm:py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                <dt class="text-sm font-medium text-gray-500">Kelurahan/Desa</dt>
                <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                  {{ $pegawai->alamat->kelurahan }}
                </dd>
              </div>
              <div class="py-4 sm:py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                <dt class="text-sm font-medium text-gray-500">Kecamatan</dt>
                <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                  {{ $pegawai->alamat->kecamatan }}
                </dd>
              </div>
              <div class="py-4 sm:py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                <dt class="text-sm font-medium text-gray-500">Kabupaten/Kota</dt>
                <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                  {{ $pegawai->alamat->kota }}
                </dd>
              </div>
              <div class="py-4 sm:py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                <dt class="text-sm font-medium text-gray-500">Provinsi</dt>
                <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                  {{ $pegawai->alamat->provinsi }}
                </dd>
              </div>
            </dl>
          </div>
        @endempty
      </div>
    </div>
  </div>
</div>
