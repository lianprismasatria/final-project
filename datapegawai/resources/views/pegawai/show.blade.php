<x-app-layout>
  <x-slot name="header">
    <h2 class="font-semibold text-xl text-gray-800 leading-tight">
      Pegawai: {{ $pegawai->nama }}
    </h2>
  </x-slot>

  @include('pegawai._basic')
  @include('pegawai._alamat')
  @include('pegawai._pendidikan')
  @include('pegawai._bpjs')
  @include('pegawai._datalain')
</x-app-layout>
