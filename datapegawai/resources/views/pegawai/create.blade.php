<x-app-layout>
  <x-slot name="header">
    <h2 class="font-semibold text-xl text-gray-800 leading-tight">Tambah Pegawai</h2>
  </x-slot>

  <div class="py-12">
    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
      <div
        class="bg-white overflow-hidden px-4 sm:px-6 lg:px-8 py-6 shadow-xl sm:rounded-lg"
      >
        <form method="post" action="{{ route('pegawai.store') }}">
          @csrf

          <div class="space-y-8 divide-y divide-gray-200 sm:space-y-5">
            <div class="space-y-8 sm:space-y-5">
              <div>
                <h3 class="text-lg leading-6 font-medium text-gray-900">
                  Data Personal
                </h3>
                <p class="mt-1 max-w-2xl text-sm text-gray-500">
                  Semua data wajib diisi. Jika bukan warga negara Indonesia, data NIK, KK, dan NPWP dapat dikosongkan.
                </p>
              </div>
              <div class="space-y-6 sm:space-y-5">
                <div
                  class="sm:grid sm:grid-cols-3 sm:gap-4 sm:items-start sm:border-t sm:border-gray-200 sm:pt-5"
                >
                  <label
                    for="nama"
                    class="block text-sm font-medium text-gray-700 sm:mt-px sm:pt-2"
                  >
                    Nama lengkap
                  </label>
                  <div class="mt-1 sm:mt-0 sm:col-span-2">
                    <input
                      type="text"
                      name="nama"
                      id="nama"
                      value="{{ old('nama') }}"
                      class="max-w-lg block w-full shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:max-w-xs sm:text-sm border-gray-300 rounded-md"
                    />
                    @error('nama')
                      <p class="mt-2 text-sm text-red-600" id="email-error">{{ $message }}</p>
                    @enderror
                  </div>
                </div>

                <div
                  class="sm:grid sm:grid-cols-3 sm:gap-4 sm:items-start sm:border-t sm:border-gray-200 sm:pt-5"
                >
                  <label
                    for="tempat_lahir"
                    class="block text-sm font-medium text-gray-700 sm:mt-px sm:pt-2"
                  >
                    Tempat lahir
                  </label>
                  <div class="mt-1 sm:mt-0 sm:col-span-2">
                    <input
                      type="text"
                      name="tempat_lahir"
                      id="tempat_lahir"
                      value="{{ old('tempat_lahir') }}"
                      class="max-w-lg block w-full shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:max-w-xs sm:text-sm border-gray-300 rounded-md"
                    />
                    @error('tempat_lahir')
                      <p class="mt-2 text-sm text-red-600" id="email-error">{{ $message }}</p>
                    @enderror
                  </div>
                </div>

                <div
                  class="sm:grid sm:grid-cols-3 sm:gap-4 sm:items-start sm:border-t sm:border-gray-200 sm:pt-5"
                >
                  <label
                    for="tanggal_lahir"
                    class="block text-sm font-medium text-gray-700 sm:mt-px sm:pt-2"
                  >
                    Tanggal lahir
                  </label>
                  <div class="mt-1 sm:mt-0 sm:col-span-2">
                    <input
                      id="tanggal_lahir"
                      name="tanggal_lahir"
                      type="date"
                      value="{{ old('tanggal_lahir') }}"
                      class="block max-w-lg w-full shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm border-gray-300 rounded-md"
                    />
                    @error('tanggal_lahir')
                      <p class="mt-2 text-sm text-red-600" id="email-error">{{ $message }}</p>
                    @enderror
                  </div>
                </div>

                <div class="space-y-6 sm:space-y-5 divide-y divide-gray-200 sm:border-t sm:border-gray-200">
                  <div class="pt-5 sm:pt-5">
                    <div role="group">
                      <div
                        class="sm:grid sm:grid-cols-3 sm:gap-4 sm:items-baseline"
                      >
                        <div>
                          <div
                            class="text-base font-medium text-gray-900 sm:text-sm sm:text-gray-700"
                            id="label-notifications"
                          >
                            Jenis kelamin
                          </div>
                        </div>
                        <div class="sm:col-span-2">
                          <div class="max-w-lg">
                            <div class="mt-4 space-y-4">
                              <div class="flex items-center">
                                <input
                                  id="laki-laki"
                                  name="jenis_kelamin"
                                  type="radio"
                                  value="Laki-laki"
                                  {{ old('jenis_kelamin')=="Laki-laki" ? 'checked' : ''}}
                                  class="focus:ring-indigo-500 h-4 w-4 text-indigo-600 border-gray-300"
                                />
                                <label
                                  for="laki-laki"
                                  class="ml-3 block text-sm font-medium text-gray-700"
                                >
                                  Laki-laki
                                </label>
                              </div>
                              <div class="flex items-center">
                                <input
                                  id="perempuan"
                                  name="jenis_kelamin"
                                  type="radio"
                                  value="Perempuan"
                                  {{ old('jenis_kelamin')=="Perempuan" ? 'checked' : ''}}
                                  class="focus:ring-indigo-500 h-4 w-4 text-indigo-600 border-gray-300"
                                />
                                <label
                                  for="perempuan"
                                  class="ml-3 block text-sm font-medium text-gray-700"
                                >
                                  Perempuan
                                </label>
                              </div>
                            </div>
                          </div>
                          @error('jenis_kelamin')
                            <p class="mt-2 text-sm text-red-600" id="email-error">{{ $message }}</p>
                          @enderror
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div
                  class="sm:grid sm:grid-cols-3 sm:gap-4 sm:items-start sm:border-t sm:border-gray-200 sm:pt-5"
                >
                  <label
                    for="golongan_darah"
                    class="block text-sm font-medium text-gray-700 sm:mt-px sm:pt-2"
                  >
                    Golongan darah
                  </label>
                  <div class="mt-1 sm:mt-0 sm:col-span-2">
                    <input
                      type="text"
                      name="golongan_darah"
                      id="golongan_darah"
                      value="{{ old('golongan_darah') }}"
                      class="max-w-lg block w-full shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:max-w-xs sm:text-sm border-gray-300 rounded-md"
                    />
                    @error('golongan_darah')
                    <p class="mt-2 text-sm text-red-600" id="email-error">{{ $message }}</p>
                    @enderror
                  </div>
                </div>

                <div class="space-y-6 sm:space-y-5 divide-y divide-gray-200 sm:border-t sm:border-gray-200">
                  <div class="pt-5 sm:pt-5">
                    <div role="group">
                      <div
                        class="sm:grid sm:grid-cols-3 sm:gap-4 sm:items-baseline"
                      >
                        <div>
                          <div
                            class="text-base font-medium text-gray-900 sm:text-sm sm:text-gray-700"
                            id="label-notifications"
                          >
                            Agama
                          </div>
                        </div>
                        <div class="sm:col-span-2">
                          <div class="max-w-lg">
                            <div class="mt-4 space-y-4">
                              <div class="flex items-center">
                                <input
                                  id="islam"
                                  name="agama"
                                  type="radio"
                                  value="Islam"
                                  {{ old('agama')=="Islam" ? 'checked' : ''}}
                                  class="focus:ring-indigo-500 h-4 w-4 text-indigo-600 border-gray-300"
                                />
                                <label
                                  for="islam"
                                  class="ml-3 block text-sm font-medium text-gray-700"
                                >
                                  Islam
                                </label>
                              </div>
                              <div class="flex items-center">
                                <input
                                  id="katolik"
                                  name="agama"
                                  type="radio"
                                  value="Katolik"
                                  {{ old('agama')=="Katolik" ? 'checked' : ''}}
                                  class="focus:ring-indigo-500 h-4 w-4 text-indigo-600 border-gray-300"
                                />
                                <label
                                  for="katolik"
                                  class="ml-3 block text-sm font-medium text-gray-700"
                                >
                                  Katolik
                                </label>
                              </div>
                              <div class="flex items-center">
                                <input
                                  id="protestan"
                                  name="agama"
                                  type="radio"
                                  value="Protestan"
                                  {{ old('agama')=="Protestan" ? 'checked' : ''}}
                                  class="focus:ring-indigo-500 h-4 w-4 text-indigo-600 border-gray-300"
                                />
                                <label
                                  for="protestan"
                                  class="ml-3 block text-sm font-medium text-gray-700"
                                >
                                  Protestan
                                </label>
                              </div>
                              <div class="flex items-center">
                                <input
                                  id="hindu"
                                  name="agama"
                                  type="radio"
                                  value="Hindu"
                                  {{ old('agama')=="Hindu" ? 'checked' : ''}}
                                  class="focus:ring-indigo-500 h-4 w-4 text-indigo-600 border-gray-300"
                                />
                                <label
                                  for="hindu"
                                  class="ml-3 block text-sm font-medium text-gray-700"
                                >
                                  Hindu
                                </label>
                              </div>
                              <div class="flex items-center">
                                <input
                                  id="budha"
                                  name="agama"
                                  type="radio"
                                  value="Budha"
                                  {{ old('agama')=="Budha" ? 'checked' : ''}}
                                  class="focus:ring-indigo-500 h-4 w-4 text-indigo-600 border-gray-300"
                                />
                                <label
                                  for="budha"
                                  class="ml-3 block text-sm font-medium text-gray-700"
                                >
                                  Budha
                                </label>
                              </div>
                              <div class="flex items-center">
                                <input
                                  id="konghucu"
                                  name="agama"
                                  type="radio"
                                  value="Konghucu"
                                  {{ old('agama')=="Konghucu" ? 'checked' : ''}}
                                  class="focus:ring-indigo-500 h-4 w-4 text-indigo-600 border-gray-300"
                                />
                                <label
                                  for="konghucu"
                                  class="ml-3 block text-sm font-medium text-gray-700"
                                >
                                  Konghucu
                                </label>
                              </div>
                              <div class="flex items-center">
                                <input
                                  id="lainnya"
                                  name="agama"
                                  type="radio"
                                  value="Lainnya"
                                  {{ old('agama')=="Lainnya" ? 'checked' : ''}}
                                  class="focus:ring-indigo-500 h-4 w-4 text-indigo-600 border-gray-300"
                                />
                                <label
                                  for="lainnya"
                                  class="ml-3 block text-sm font-medium text-gray-700"
                                >
                                  Lainnya
                                </label>
                              </div>
                            </div>
                          </div>
                          @error('agama')
                            <p class="mt-2 text-sm text-red-600" id="email-error">{{ $message }}</p>
                          @enderror
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="space-y-6 sm:space-y-5 divide-y divide-gray-200 sm:border-t sm:border-gray-200">
                  <div class="pt-5 sm:pt-5">
                    <div role="group">
                      <div
                        class="sm:grid sm:grid-cols-3 sm:gap-4 sm:items-baseline"
                      >
                        <div>
                          <div
                            class="text-base font-medium text-gray-900 sm:text-sm sm:text-gray-700"
                            id="label-notifications"
                          >
                            Status perkawinan
                          </div>
                        </div>
                        <div class="sm:col-span-2">
                          <div class="max-w-lg">
                            <div class="mt-4 space-y-4">
                              <div class="flex items-center">
                                <input
                                  id="belum_kawin"
                                  name="status_perkawinan"
                                  type="radio"
                                  value="Belum kawin"
                                  {{ old('status_perkawinan')=="Belum kawin" ? 'checked' : ''}}
                                  class="focus:ring-indigo-500 h-4 w-4 text-indigo-600 border-gray-300"
                                />
                                <label
                                  for="belum_kawin"
                                  class="ml-3 block text-sm font-medium text-gray-700"
                                >
                                  Belum Kawin
                                </label>
                              </div>
                              <div class="flex items-center">
                                <input
                                  id="kawin"
                                  name="status_perkawinan"
                                  type="radio"
                                  value="Kawin"
                                  {{ old('status_perkawinan')=="Kawin" ? 'checked' : ''}}
                                  class="focus:ring-indigo-500 h-4 w-4 text-indigo-600 border-gray-300"
                                />
                                <label
                                  for="kawin"
                                  class="ml-3 block text-sm font-medium text-gray-700"
                                >
                                  Kawin
                                </label>
                              </div>
                              <div class="flex items-center">
                                <input
                                  id="janda"
                                  name="status_perkawinan"
                                  type="radio"
                                  value="Janda"
                                  {{ old('status_perkawinan')=="Janda" ? 'checked' : ''}}
                                  class="focus:ring-indigo-500 h-4 w-4 text-indigo-600 border-gray-300"
                                />
                                <label
                                  for="janda"
                                  class="ml-3 block text-sm font-medium text-gray-700"
                                >
                                  Janda
                                </label>
                              </div>
                              <div class="flex items-center">
                                <input
                                  id="duda"
                                  name="status_perkawinan"
                                  type="radio"
                                  value="Duda"
                                  {{ old('status_perkawinan')=="Duda" ? 'checked' : ''}}
                                  class="focus:ring-indigo-500 h-4 w-4 text-indigo-600 border-gray-300"
                                />
                                <label
                                  for="duda"
                                  class="ml-3 block text-sm font-medium text-gray-700"
                                >
                                  Duda
                                </label>
                              </div>
                            </div>
                          </div>
                          @error('status_perkawinan')
                            <p class="mt-2 text-sm text-red-600" id="email-error">{{ $message }}</p>
                          @enderror
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div
                  class="sm:grid sm:grid-cols-3 sm:gap-4 sm:items-start sm:border-t sm:border-gray-200 sm:pt-5"
                >
                  <label
                    for="kewarganegaraan"
                    class="block text-sm font-medium text-gray-700 sm:mt-px sm:pt-2"
                  >
                    Kewarganegaraan
                  </label>
                  <div class="mt-1 sm:mt-0 sm:col-span-2">
                    <input
                      type="text"
                      name="kewarganegaraan"
                      id="kewarganegaraan"
                      value="{{ old('kewarganegaraan') }}"
                      class="max-w-lg block w-full shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:max-w-xs sm:text-sm border-gray-300 rounded-md"
                    />
                    @error('kewarganegaraan')
                      <p class="mt-2 text-sm text-red-600" id="email-error">{{ $message }}</p>
                    @enderror
                  </div>
                </div>
                
                <div
                  class="sm:grid sm:grid-cols-3 sm:gap-4 sm:items-start sm:border-t sm:border-gray-200 sm:pt-5"
                >
                  <label
                    for="nik"
                    class="block text-sm font-medium text-gray-700 sm:mt-px sm:pt-2"
                  >
                    Nomor Induk Kependudukan (NIK)
                  </label>
                  <div class="mt-1 sm:mt-0 sm:col-span-2">
                    <input
                      type="text"
                      name="nik"
                      id="nik"
                      value="{{ old('nik') }}"
                      class="max-w-lg block w-full shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:max-w-xs sm:text-sm border-gray-300 rounded-md"
                    />
                    @error('nik')
                      <p class="mt-2 text-sm text-red-600" id="email-error">{{ $message }}</p>
                    @enderror
                  </div>
                </div>

                <div
                  class="sm:grid sm:grid-cols-3 sm:gap-4 sm:items-start sm:border-t sm:border-gray-200 sm:pt-5"
                >
                  <label
                    for="kk"
                    class="block text-sm font-medium text-gray-700 sm:mt-px sm:pt-2"
                  >
                    Nomor Kartu Keluarga (KK)
                  </label>
                  <div class="mt-1 sm:mt-0 sm:col-span-2">
                    <input
                      type="text"
                      name="kk"
                      id="kk"
                      value="{{ old('kk') }}"
                      class="max-w-lg block w-full shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:max-w-xs sm:text-sm border-gray-300 rounded-md"
                    />
                    @error('kk')
                      <p class="mt-2 text-sm text-red-600" id="email-error">{{ $message }}</p>
                    @enderror
                  </div>
                </div>

                <div
                  class="sm:grid sm:grid-cols-3 sm:gap-4 sm:items-start sm:border-t sm:border-gray-200 sm:pt-5"
                >
                  <label
                    for="npwp"
                    class="block text-sm font-medium text-gray-700 sm:mt-px sm:pt-2"
                  >
                    Nomor Pokok Wajib Pajak (NPWP)
                  </label>
                  <div class="mt-1 sm:mt-0 sm:col-span-2">
                    <input
                      type="text"
                      name="npwp"
                      id="npwp"
                      value="{{ old('npwp ') }}"
                      class="max-w-lg block w-full shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:max-w-xs sm:text-sm border-gray-300 rounded-md"
                    />
                    @error('npwp')
                      <p class="mt-2 text-sm text-red-600" id="email-error">{{ $message }}</p>
                    @enderror
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="pt-5">
            <div class="flex justify-end">
              <a href="{{ route('pegawai.index') }}"
                class="bg-white py-2 px-4 border border-gray-300 rounded-md shadow-sm text-sm font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
              >Cancel</a>
              <button
                type="submit"
                class="ml-3 inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
              >
                Save
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</x-app-layout>
