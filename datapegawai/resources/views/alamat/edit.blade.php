<x-app-layout>
  <x-slot name="header">
    <h2 class="font-semibold text-xl text-gray-800 leading-tight">Edit Alamat Pegawai: {{ $alamat->pegawai->nama }}</h2>
  </x-slot>

  <div class="py-12">
    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
      <div
        class="bg-white overflow-hidden px-4 sm:px-6 lg:px-8 py-6 shadow-xl sm:rounded-lg"
      >
        <form method="post" action="{{ route('pegawai.alamat.update', ['pegawai' => $alamat->pegawai_id]) }}">
          @method('put')
          @csrf

          <div class="space-y-8 divide-y divide-gray-200 sm:space-y-5">
            <div class="space-y-8 sm:space-y-5">
              <div>
                <h3 class="text-lg leading-6 font-medium text-gray-900">
                  Data Alamat
                </h3>
                <p class="mt-1 max-w-2xl text-sm text-gray-500">
                  Isikan alamat sesuai tempat tinggal sekarang.
                </p>
              </div>
              <div class="space-y-6 sm:space-y-5">
                <div
                  class="sm:grid sm:grid-cols-3 sm:gap-4 sm:items-start sm:border-t sm:border-gray-200 sm:pt-5"
                >
                  <label
                    for="jalan_nomor"
                    class="block text-sm font-medium text-gray-700 sm:mt-px sm:pt-2"
                  >
                    Nama jalan, komplek dan nomor rumah
                  </label>
                  <div class="mt-1 sm:mt-0 sm:col-span-2">
                    <input
                      type="text"
                      name="jalan_nomor"
                      id="jalan_nomor"
                      value="{{ $alamat->jalan_nomor }}"
                      class="max-w-lg block w-full shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:max-w-xs sm:text-sm border-gray-300 rounded-md"
                    />
                    @error('jalan_nomor')
                      <p class="mt-2 text-sm text-red-600" id="email-error">{{ $message }}</p>
                    @enderror
                  </div>
                </div>

                <div
                  class="sm:grid sm:grid-cols-3 sm:gap-4 sm:items-start sm:border-t sm:border-gray-200 sm:pt-5"
                >
                  <label
                    for="rt"
                    class="block text-sm font-medium text-gray-700 sm:mt-px sm:pt-2"
                  >
                    RT
                  </label>
                  <div class="mt-1 sm:mt-0 sm:col-span-2">
                    <input
                      type="text"
                      name="rt"
                      id="rt"
                      value="{{ $alamat->rt }}"
                      class="max-w-lg block w-full shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:max-w-xs sm:text-sm border-gray-300 rounded-md"
                    />
                    @error('rt')
                      <p class="mt-2 text-sm text-red-600" id="email-error">{{ $message }}</p>
                    @enderror
                  </div>
                </div>

                <div
                  class="sm:grid sm:grid-cols-3 sm:gap-4 sm:items-start sm:border-t sm:border-gray-200 sm:pt-5"
                >
                  <label
                    for="rw"
                    class="block text-sm font-medium text-gray-700 sm:mt-px sm:pt-2"
                  >
                    RW
                  </label>
                  <div class="mt-1 sm:mt-0 sm:col-span-2">
                    <input
                      type="text"
                      name="rw"
                      id="rw"
                      value="{{ $alamat->rw }}"
                      class="max-w-lg block w-full shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:max-w-xs sm:text-sm border-gray-300 rounded-md"
                    />
                    @error('rw')
                      <p class="mt-2 text-sm text-red-600" id="email-error">{{ $message }}</p>
                    @enderror
                  </div>
                </div>

                <div
                  class="sm:grid sm:grid-cols-3 sm:gap-4 sm:items-start sm:border-t sm:border-gray-200 sm:pt-5"
                >
                  <label
                    for="kelurahan"
                    class="block text-sm font-medium text-gray-700 sm:mt-px sm:pt-2"
                  >
                    Kelurahan/Desa
                  </label>
                  <div class="mt-1 sm:mt-0 sm:col-span-2">
                    <input
                      type="text"
                      name="kelurahan"
                      id="kelurahan"
                      value="{{ $alamat->kelurahan }}"
                      class="max-w-lg block w-full shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:max-w-xs sm:text-sm border-gray-300 rounded-md"
                    />
                    @error('kelurahan')
                      <p class="mt-2 text-sm text-red-600" id="email-error">{{ $message }}</p>
                    @enderror
                  </div>
                </div>

                <div
                  class="sm:grid sm:grid-cols-3 sm:gap-4 sm:items-start sm:border-t sm:border-gray-200 sm:pt-5"
                >
                  <label
                    for="kecamatan"
                    class="block text-sm font-medium text-gray-700 sm:mt-px sm:pt-2"
                  >
                    Kelurahan/Desa
                  </label>
                  <div class="mt-1 sm:mt-0 sm:col-span-2">
                    <input
                      type="text"
                      name="kecamatan"
                      id="kecamatan"
                      value="{{ $alamat->kecamatan }}"
                      class="max-w-lg block w-full shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:max-w-xs sm:text-sm border-gray-300 rounded-md"
                    />
                    @error('kecamatan')
                      <p class="mt-2 text-sm text-red-600" id="email-error">{{ $message }}</p>
                    @enderror
                  </div>
                </div>

                <div
                  class="sm:grid sm:grid-cols-3 sm:gap-4 sm:items-start sm:border-t sm:border-gray-200 sm:pt-5"
                >
                  <label
                    for="kota"
                    class="block text-sm font-medium text-gray-700 sm:mt-px sm:pt-2"
                  >
                    Kabupaten/Kota
                  </label>
                  <div class="mt-1 sm:mt-0 sm:col-span-2">
                    <input
                      type="text"
                      name="kota"
                      id="kota"
                      value="{{ $alamat->kota }}"
                      class="max-w-lg block w-full shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:max-w-xs sm:text-sm border-gray-300 rounded-md"
                    />
                    @error('kota')
                      <p class="mt-2 text-sm text-red-600" id="email-error">{{ $message }}</p>
                    @enderror
                  </div>
                </div>

                <div
                  class="sm:grid sm:grid-cols-3 sm:gap-4 sm:items-start sm:border-t sm:border-gray-200 sm:pt-5"
                >
                  <label
                    for="provinsi"
                    class="block text-sm font-medium text-gray-700 sm:mt-px sm:pt-2"
                  >
                    Provinsi
                  </label>
                  <div class="mt-1 sm:mt-0 sm:col-span-2">
                    <input
                      type="text"
                      name="provinsi"
                      id="provinsi"
                      value="{{ $alamat->provinsi }}"
                      class="max-w-lg block w-full shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:max-w-xs sm:text-sm border-gray-300 rounded-md"
                    />
                    @error('provinsi')
                      <p class="mt-2 text-sm text-red-600" id="email-error">{{ $message }}</p>
                    @enderror
                  </div>
                </div>

              </div>
            </div>
          </div>

          <div class="pt-5">
            <div class="flex justify-end">
              <a href="{{ route('pegawai.show', ['pegawai' => $alamat->pegawai_id]) }}"
                class="bg-white py-2 px-4 border border-gray-300 rounded-md shadow-sm text-sm font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
              >Cancel</a>
              <button
                type="submit"
                class="ml-3 inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
              >
                Save
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</x-app-layout>
