<x-app-layout>
  <x-slot name="header">
    <h2 class="font-semibold text-xl text-gray-800 leading-tight">Input Data Pendidikan Pegawai: {{ $pegawai->nama }}</h2>
  </x-slot>

  <div class="py-12">
    <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
      <div
        class="bg-white overflow-hidden px-4 sm:px-6 lg:px-8 py-6 shadow-xl sm:rounded-lg"
      >
        <form method="post" action="{{ route('pegawai.pendidikan.store', ['pegawai' => $pegawai]) }}">
          @csrf

          <div class="space-y-8 divide-y divide-gray-200 sm:space-y-5">
            <div class="space-y-8 sm:space-y-5">
              <div>
                <h3 class="text-lg leading-6 font-medium text-gray-900">
                  Data pendidikan
                </h3>
                <p class="mt-1 max-w-2xl text-sm text-gray-500">
                  Semua data wajib diisi.
                </p>
              </div>
              <input type="hidden" id="pegawai_id" name="pegawai_id" value="{{ $pegawai->id }}">
              <div class="space-y-6 sm:space-y-5">
                <div
                  class="sm:grid sm:grid-cols-3 sm:gap-4 sm:items-start sm:border-t sm:border-gray-200 sm:pt-5"
                >
                  <label
                    for="jenjang"
                    class="block text-sm font-medium text-gray-700 sm:mt-px sm:pt-2"
                  >
                    Jenjang
                  </label>
                  <div class="mt-1 sm:mt-0 sm:col-span-2">
                    <input
                      type="text"
                      name="jenjang"
                      id="jenjang"
                      value="{{ old('jenjang') }}"
                      class="max-w-lg block w-full shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:max-w-xs sm:text-sm border-gray-300 rounded-md"
                    />
                    @error('jenjang')
                      <p class="mt-2 text-sm text-red-600" id="email-error">{{ $message }}</p>
                    @enderror
                  </div>
                </div>

                <div
                  class="sm:grid sm:grid-cols-3 sm:gap-4 sm:items-start sm:border-t sm:border-gray-200 sm:pt-5"
                >
                  <label
                    for="nama_sekolah"
                    class="block text-sm font-medium text-gray-700 sm:mt-px sm:pt-2"
                  >
                    Nama sekolah/perguruan tinggi
                  </label>
                  <div class="mt-1 sm:mt-0 sm:col-span-2">
                    <input
                      type="text"
                      name="nama_sekolah"
                      id="nama_sekolah"
                      value="{{ old('nama_sekolah') }}"
                      class="max-w-lg block w-full shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:max-w-xs sm:text-sm border-gray-300 rounded-md"
                    />
                    @error('nama_sekolah')
                      <p class="mt-2 text-sm text-red-600" id="email-error">{{ $message }}</p>
                    @enderror
                  </div>
                </div>

                <div
                  class="sm:grid sm:grid-cols-3 sm:gap-4 sm:items-start sm:border-t sm:border-gray-200 sm:pt-5"
                >
                  <label
                    for="jurusan"
                    class="block text-sm font-medium text-gray-700 sm:mt-px sm:pt-2"
                  >
                    Jurusan
                  </label>
                  <div class="mt-1 sm:mt-0 sm:col-span-2">
                    <input
                      type="text"
                      name="jurusan"
                      id="jurusan"
                      value="{{ old('jurusan') }}"
                      class="max-w-lg block w-full shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:max-w-xs sm:text-sm border-gray-300 rounded-md"
                    />
                    @error('jurusan')
                      <p class="mt-2 text-sm text-red-600" id="email-error">{{ $message }}</p>
                    @enderror
                  </div>
                </div>

                <div
                  class="sm:grid sm:grid-cols-3 sm:gap-4 sm:items-start sm:border-t sm:border-gray-200 sm:pt-5"
                >
                  <label
                    for="tahun_lulus"
                    class="block text-sm font-medium text-gray-700 sm:mt-px sm:pt-2"
                  >
                    Tahun lulus
                  </label>
                  <div class="mt-1 sm:mt-0 sm:col-span-2">
                    <input
                      type="text"
                      name="tahun_lulus"
                      id="tahun_lulus"
                      value="{{ old('tahun_lulus') }}"
                      class="max-w-lg block w-full shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:max-w-xs sm:text-sm border-gray-300 rounded-md"
                    />
                    @error('tahun_lulus')
                      <p class="mt-2 text-sm text-red-600" id="email-error">{{ $message }}</p>
                    @enderror
                  </div>
                </div>

                <div
                  class="sm:grid sm:grid-cols-3 sm:gap-4 sm:items-start sm:border-t sm:border-gray-200 sm:pt-5"
                >
                  <label
                    for="nomor_ijazah"
                    class="block text-sm font-medium text-gray-700 sm:mt-px sm:pt-2"
                  >
                    Nomor ijazah
                  </label>
                  <div class="mt-1 sm:mt-0 sm:col-span-2">
                    <input
                      type="text"
                      name="nomor_ijazah"
                      id="nomor_ijazah"
                      value="{{ old('nomor_ijazah') }}"
                      class="max-w-lg block w-full shadow-sm focus:ring-indigo-500 focus:border-indigo-500 sm:max-w-xs sm:text-sm border-gray-300 rounded-md"
                    />
                    @error('nomor_ijazah')
                      <p class="mt-2 text-sm text-red-600" id="email-error">{{ $message }}</p>
                    @enderror
                  </div>
                </div>

              </div>
            </div>
          </div>

          <div class="pt-5">
            <div class="flex justify-end">
              <a href="{{ route('pegawai.show', ['pegawai' => $pegawai]) }}"
                class="bg-white py-2 px-4 border border-gray-300 rounded-md shadow-sm text-sm font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
              >Cancel</a>
              <button
                type="submit"
                class="ml-3 inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500"
              >
                Save
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</x-app-layout>
