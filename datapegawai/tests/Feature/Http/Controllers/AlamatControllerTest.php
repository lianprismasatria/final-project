<?php

namespace Tests\Feature\Http\Controllers;

use App\Models\Alamat;
use App\Models\Pegawai;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use JMac\Testing\Traits\AdditionalAssertions;
use Tests\TestCase;

/**
 * @see \App\Http\Controllers\AlamatController
 */
class AlamatControllerTest extends TestCase
{
    use AdditionalAssertions, RefreshDatabase, WithFaker;

    /**
     * @test
     */
    public function create_displays_view()
    {
        $alamat = Pegawai::factory()->create();

        $response = $this->get(route('alamat.create'));

        $response->assertOk();
        $response->assertViewIs('alamat.create');
    }


    /**
     * @test
     */
    public function store_uses_form_request_validation()
    {
        $this->assertActionUsesFormRequest(
            \App\Http\Controllers\AlamatController::class,
            'store',
            \App\Http\Requests\AlamatStoreRequest::class
        );
    }

    /**
     * @test
     */
    public function store_saves_and_redirects()
    {
        $pegawai = Pegawai::factory()->create();
        $jalan_nomor = $this->faker->word;
        $rt = $this->faker->word;
        $rw = $this->faker->word;
        $kelurahan = $this->faker->word;
        $kecamatan = $this->faker->word;
        $kota = $this->faker->word;
        $provinsi = $this->faker->word;

        $response = $this->post(route('alamat.store'), [
            'pegawai_id' => $pegawai->id,
            'jalan_nomor' => $jalan_nomor,
            'rt' => $rt,
            'rw' => $rw,
            'kelurahan' => $kelurahan,
            'kecamatan' => $kecamatan,
            'kota' => $kota,
            'provinsi' => $provinsi,
        ]);

        $alamats = Alamat::query()
            ->where('pegawai_id', $pegawai->id)
            ->where('jalan_nomor', $jalan_nomor)
            ->where('rt', $rt)
            ->where('rw', $rw)
            ->where('kelurahan', $kelurahan)
            ->where('kecamatan', $kecamatan)
            ->where('kota', $kota)
            ->where('provinsi', $provinsi)
            ->get();
        $this->assertCount(1, $alamats);
        $alamat = $alamats->first();

        $response->assertRedirect(route('pegawai.show', ['pegawai' => $pegawai]));
    }


    /**
     * @test
     */
    public function edit_displays_view()
    {
        $alamat = Alamat::factory()->create();

        $response = $this->get(route('alamat.edit', $alamat));

        $response->assertOk();
        $response->assertViewIs('alamat.edit');
        $response->assertViewHas('alamat');
    }


    /**
     * @test
     */
    public function update_uses_form_request_validation()
    {
        $this->assertActionUsesFormRequest(
            \App\Http\Controllers\AlamatController::class,
            'update',
            \App\Http\Requests\AlamatUpdateRequest::class
        );
    }

    /**
     * @test
     */
    public function update_redirects()
    {
        $alamat = Alamat::factory()->create();
        $pegawai = Pegawai::factory()->create();
        $jalan_nomor = $this->faker->word;
        $rt = $this->faker->word;
        $rw = $this->faker->word;
        $kelurahan = $this->faker->word;
        $kecamatan = $this->faker->word;
        $kota = $this->faker->word;
        $provinsi = $this->faker->word;

        $response = $this->put(route('alamat.update', $alamat), [
            'pegawai_id' => $pegawai->id,
            'jalan_nomor' => $jalan_nomor,
            'rt' => $rt,
            'rw' => $rw,
            'kelurahan' => $kelurahan,
            'kecamatan' => $kecamatan,
            'kota' => $kota,
            'provinsi' => $provinsi,
        ]);

        $alamat->refresh();

        $response->assertRedirect(route('pegawai.show', ['pegawai' => $pegawai]));

        $this->assertEquals($pegawai->id, $alamat->pegawai_id);
        $this->assertEquals($jalan_nomor, $alamat->jalan_nomor);
        $this->assertEquals($rt, $alamat->rt);
        $this->assertEquals($rw, $alamat->rw);
        $this->assertEquals($kelurahan, $alamat->kelurahan);
        $this->assertEquals($kecamatan, $alamat->kecamatan);
        $this->assertEquals($kota, $alamat->kota);
        $this->assertEquals($provinsi, $alamat->provinsi);
    }


    /**
     * @test
     */
    public function destroy_deletes_and_redirects()
    {
        $alamat = Alamat::factory()->create();

        $response = $this->delete(route('alamat.destroy', $alamat));

        $response->assertRedirect(route('pegawai.show', ['pegawai' => $pegawai]));

        $this->assertDeleted($alamat);
    }
}
