<?php

namespace Tests\Feature\Http\Controllers;

use App\Models\Pegawai;
use App\Models\Pendidikan;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use JMac\Testing\Traits\AdditionalAssertions;
use Tests\TestCase;

/**
 * @see \App\Http\Controllers\PendidikanController
 */
class PendidikanControllerTest extends TestCase
{
    use AdditionalAssertions, RefreshDatabase, WithFaker;

    /**
     * @test
     */
    public function create_displays_view()
    {
        $pendidikan = Pegawai::factory()->create();

        $response = $this->get(route('pendidikan.create'));

        $response->assertOk();
        $response->assertViewIs('pendidikan.create');
    }


    /**
     * @test
     */
    public function store_uses_form_request_validation()
    {
        $this->assertActionUsesFormRequest(
            \App\Http\Controllers\PendidikanController::class,
            'store',
            \App\Http\Requests\PendidikanStoreRequest::class
        );
    }

    /**
     * @test
     */
    public function store_saves_and_redirects()
    {
        $pegawai = Pegawai::factory()->create();
        $jenjang = $this->faker->word;
        $nama_sekolah = $this->faker->word;
        $jurusan = $this->faker->word;
        $tanggal_lulus = $this->faker->date();
        $nomor_ijazah = $this->faker->word;

        $response = $this->post(route('pendidikan.store'), [
            'pegawai_id' => $pegawai->id,
            'jenjang' => $jenjang,
            'nama_sekolah' => $nama_sekolah,
            'jurusan' => $jurusan,
            'tanggal_lulus' => $tanggal_lulus,
            'nomor_ijazah' => $nomor_ijazah,
        ]);

        $pendidikans = Pendidikan::query()
            ->where('pegawai_id', $pegawai->id)
            ->where('jenjang', $jenjang)
            ->where('nama_sekolah', $nama_sekolah)
            ->where('jurusan', $jurusan)
            ->where('tanggal_lulus', $tanggal_lulus)
            ->where('nomor_ijazah', $nomor_ijazah)
            ->get();
        $this->assertCount(1, $pendidikans);
        $pendidikan = $pendidikans->first();

        $response->assertRedirect(route('pegawai.show', ['pegawai' => $pegawai]));
    }


    /**
     * @test
     */
    public function edit_displays_view()
    {
        $pendidikan = Pendidikan::factory()->create();

        $response = $this->get(route('pendidikan.edit', $pendidikan));

        $response->assertOk();
        $response->assertViewIs('pendidikan.edit');
        $response->assertViewHas('pendidikan');
    }


    /**
     * @test
     */
    public function update_uses_form_request_validation()
    {
        $this->assertActionUsesFormRequest(
            \App\Http\Controllers\PendidikanController::class,
            'update',
            \App\Http\Requests\PendidikanUpdateRequest::class
        );
    }

    /**
     * @test
     */
    public function update_redirects()
    {
        $pendidikan = Pendidikan::factory()->create();
        $pegawai = Pegawai::factory()->create();
        $jenjang = $this->faker->word;
        $nama_sekolah = $this->faker->word;
        $jurusan = $this->faker->word;
        $tanggal_lulus = $this->faker->date();
        $nomor_ijazah = $this->faker->word;

        $response = $this->put(route('pendidikan.update', $pendidikan), [
            'pegawai_id' => $pegawai->id,
            'jenjang' => $jenjang,
            'nama_sekolah' => $nama_sekolah,
            'jurusan' => $jurusan,
            'tanggal_lulus' => $tanggal_lulus,
            'nomor_ijazah' => $nomor_ijazah,
        ]);

        $pendidikan->refresh();

        $response->assertRedirect(route('pegawai.show', ['pegawai' => $pegawai]));

        $this->assertEquals($pegawai->id, $pendidikan->pegawai_id);
        $this->assertEquals($jenjang, $pendidikan->jenjang);
        $this->assertEquals($nama_sekolah, $pendidikan->nama_sekolah);
        $this->assertEquals($jurusan, $pendidikan->jurusan);
        $this->assertEquals(Carbon::parse($tanggal_lulus), $pendidikan->tanggal_lulus);
        $this->assertEquals($nomor_ijazah, $pendidikan->nomor_ijazah);
    }


    /**
     * @test
     */
    public function destroy_deletes_and_redirects()
    {
        $pendidikan = Pendidikan::factory()->create();

        $response = $this->delete(route('pendidikan.destroy', $pendidikan));

        $response->assertRedirect(route('pegawai.show', ['pegawai' => $pegawai]));

        $this->assertDeleted($pendidikan);
    }
}
