<?php

namespace Tests\Feature\Http\Controllers;

use App\Models\Pegawai;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use JMac\Testing\Traits\AdditionalAssertions;
use Tests\TestCase;

/**
 * @see \App\Http\Controllers\PegawaiController
 */
class PegawaiControllerTest extends TestCase
{
    use AdditionalAssertions, RefreshDatabase, WithFaker;

    /**
     * @test
     */
    public function index_displays_view()
    {
        $pegawais = Pegawai::factory()->count(3)->create();

        $response = $this->get(route('pegawai.index'));

        $response->assertOk();
        $response->assertViewIs('pegawai.index');
        $response->assertViewHas('pegawais');
    }


    /**
     * @test
     */
    public function create_displays_view()
    {
        $response = $this->get(route('pegawai.create'));

        $response->assertOk();
        $response->assertViewIs('pegawai.create');
    }


    /**
     * @test
     */
    public function store_uses_form_request_validation()
    {
        $this->assertActionUsesFormRequest(
            \App\Http\Controllers\PegawaiController::class,
            'store',
            \App\Http\Requests\PegawaiStoreRequest::class
        );
    }

    /**
     * @test
     */
    public function store_saves_and_redirects()
    {
        $nama = $this->faker->word;
        $tempat_lahir = $this->faker->word;
        $tanggal_lahir = $this->faker->date();
        $jenis_kelamin = $this->faker->word;
        $golongan_darah = $this->faker->word;
        $agama = $this->faker->word;
        $status_perkawinan = $this->faker->word;
        $kewarganegaraan = $this->faker->word;
        $nik = $this->faker->word;
        $kk = $this->faker->word;
        $npwp = $this->faker->word;

        $response = $this->post(route('pegawai.store'), [
            'nama' => $nama,
            'tempat_lahir' => $tempat_lahir,
            'tanggal_lahir' => $tanggal_lahir,
            'jenis_kelamin' => $jenis_kelamin,
            'golongan_darah' => $golongan_darah,
            'agama' => $agama,
            'status_perkawinan' => $status_perkawinan,
            'kewarganegaraan' => $kewarganegaraan,
            'nik' => $nik,
            'kk' => $kk,
            'npwp' => $npwp,
        ]);

        $pegawais = Pegawai::query()
            ->where('nama', $nama)
            ->where('tempat_lahir', $tempat_lahir)
            ->where('tanggal_lahir', $tanggal_lahir)
            ->where('jenis_kelamin', $jenis_kelamin)
            ->where('golongan_darah', $golongan_darah)
            ->where('agama', $agama)
            ->where('status_perkawinan', $status_perkawinan)
            ->where('kewarganegaraan', $kewarganegaraan)
            ->where('nik', $nik)
            ->where('kk', $kk)
            ->where('npwp', $npwp)
            ->get();
        $this->assertCount(1, $pegawais);
        $pegawai = $pegawais->first();

        $response->assertRedirect(route('pegawai.index'));
    }


    /**
     * @test
     */
    public function show_displays_view()
    {
        $pegawai = Pegawai::factory()->create();

        $response = $this->get(route('pegawai.show', $pegawai));

        $response->assertOk();
        $response->assertViewIs('pegawai.show');
        $response->assertViewHas('pegawai');
    }


    /**
     * @test
     */
    public function edit_displays_view()
    {
        $pegawai = Pegawai::factory()->create();

        $response = $this->get(route('pegawai.edit', $pegawai));

        $response->assertOk();
        $response->assertViewIs('pegawai.edit');
        $response->assertViewHas('pegawai');
    }


    /**
     * @test
     */
    public function update_uses_form_request_validation()
    {
        $this->assertActionUsesFormRequest(
            \App\Http\Controllers\PegawaiController::class,
            'update',
            \App\Http\Requests\PegawaiUpdateRequest::class
        );
    }

    /**
     * @test
     */
    public function update_redirects()
    {
        $pegawai = Pegawai::factory()->create();
        $nama = $this->faker->word;
        $tempat_lahir = $this->faker->word;
        $tanggal_lahir = $this->faker->date();
        $jenis_kelamin = $this->faker->word;
        $golongan_darah = $this->faker->word;
        $agama = $this->faker->word;
        $status_perkawinan = $this->faker->word;
        $kewarganegaraan = $this->faker->word;
        $nik = $this->faker->word;
        $kk = $this->faker->word;
        $npwp = $this->faker->word;

        $response = $this->put(route('pegawai.update', $pegawai), [
            'nama' => $nama,
            'tempat_lahir' => $tempat_lahir,
            'tanggal_lahir' => $tanggal_lahir,
            'jenis_kelamin' => $jenis_kelamin,
            'golongan_darah' => $golongan_darah,
            'agama' => $agama,
            'status_perkawinan' => $status_perkawinan,
            'kewarganegaraan' => $kewarganegaraan,
            'nik' => $nik,
            'kk' => $kk,
            'npwp' => $npwp,
        ]);

        $pegawai->refresh();

        $response->assertRedirect(route('pegawai.show', ['pegawai' => $pegawai]));

        $this->assertEquals($nama, $pegawai->nama);
        $this->assertEquals($tempat_lahir, $pegawai->tempat_lahir);
        $this->assertEquals(Carbon::parse($tanggal_lahir), $pegawai->tanggal_lahir);
        $this->assertEquals($jenis_kelamin, $pegawai->jenis_kelamin);
        $this->assertEquals($golongan_darah, $pegawai->golongan_darah);
        $this->assertEquals($agama, $pegawai->agama);
        $this->assertEquals($status_perkawinan, $pegawai->status_perkawinan);
        $this->assertEquals($kewarganegaraan, $pegawai->kewarganegaraan);
        $this->assertEquals($nik, $pegawai->nik);
        $this->assertEquals($kk, $pegawai->kk);
        $this->assertEquals($npwp, $pegawai->npwp);
    }


    /**
     * @test
     */
    public function destroy_deletes_and_redirects()
    {
        $pegawai = Pegawai::factory()->create();

        $response = $this->delete(route('pegawai.destroy', $pegawai));

        $response->assertRedirect(route('pegawai.index'));

        $this->assertDeleted($pegawai);
    }
}
